const $config = require('config');
const $Telegraf = require('telegraf');
const $log = require('./src/libs/log');

const TOKEN = $config.get('bot.token');

$log.info('Init bot with token = %s', TOKEN);

const bot = new $Telegraf(TOKEN);
const handlers = $config.get('handlers');

handlers.forEach((name) => {
  require('./src/handlers/' + name)(bot);
});

bot.startPolling();

$log.info('Bot started polling!');
