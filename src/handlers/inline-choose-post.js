const $API = require('../modules/api');
const $log = require('../libs/log');
const $config = require('config');

const shortHost = $config.get('services.shortHost.url');
const DTPHost = $config.get('services.dtp.url');

module.exports = function (bot) {
  bot.on('inline_query', (ctx) => {
    const query = ctx.inlineQuery.query;
    const id = (query.match(/.+\/(.+)+$/) || []).pop();

    if (!id) {
      return;
    }

    $API.query('getPost/' + id, {
      return_content: false
    })
      .then(async (result) => {
        if (!result.ok) {
          throw result;
        }

        const data = result.result;
        const title = data.preview.title;
        const description = data.preview.description;
        const image = data.preview.image_url;
        const postLink = DTPHost + '/' + id;
        const shortLink = shortHost.replace(/http(s)?:\/\//, '') + '/' + id;
        const ivLink = `https://t.me/iv?url=${encodeURIComponent(postLink)}&rhash=7c83b6ab2dc887`;

        ctx.answerInlineQuery([
          {
            type: 'article',
            title,
            description,
            id: 1,
            thumb_url: image,
            input_message_content: {
              message_text: `[${title}](${ivLink})\n\n${description}\n\n${shortLink}`,
              parse_mode: 'Markdown'
            }
          }
        ], {
          cache_time: 360
        });
      })
      .catch((err) => {
        $log.error(err);
      })
  });
};