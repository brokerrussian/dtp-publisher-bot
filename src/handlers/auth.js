const $Users = require('../modules/users');
const $ctx = require('../modules/ctx');
const $log = require('../libs/log');

module.exports = function (bot) {
  bot.use((ctx, next) => {
    const query = $ctx.parse(ctx);

    $Users.setSession(query.user)
      .then(() => {
        next(ctx);
      })
      .catch((err) => {
        $log.error(err);
        ctx.reply('Oops.. Happened error!');
      });
  });
};