const $Posts = require('../modules/posts');
const $API = require('../modules/api');
const $Users = require('../modules/users');
const $ctx = require('../modules/ctx');
const $log = require('../libs/log');
const $TelegraphAPI = require('../modules/telegraph-api');
const $Telegram = require('telegraf/telegram');
const $config = require('config');
const Markup = require('telegraf/markup');
const Extra = require('telegraf/extra');
const _ = require('lodash');

const telegram = new $Telegram($config.get('bot.token'));

const hashLanguage = {
  ru: 'Russian',
  en: 'English',
  uk: 'Ukrainian',
  fr: 'French',
  it: 'Italian',
  es: 'Spanish',
  nl: 'Dutch',
  other: 'Other'
};

const CMD_SEPARATOR = '<!-!>';
const shortHost = $config.get('services.shortHost.url').replace(/http(s)?:\/\//, '');;

module.exports = function (app) {
  const postLinkPattern = /(http(s)?:\/\/)?(telegra\.ph|graph\.org)\/([a-zA-Z-0-9]+)/;

  app.hears(postLinkPattern, (ctx) => {
    const query = $ctx.parse(ctx);
    const link = query.message.text.match(postLinkPattern)[0];
    const user_id = query.user.id;
    const path = link.match(postLinkPattern).pop();

    let draftId;

    $Posts.deleteAllDrafts(user_id)
      .then(async () => {
        const token = await $Users.getToken(user_id);
        const checkPost = await $API.query('checkPost/' + path, {
          token
        });

        if (!checkPost.ok) {
          if (checkPost.error.code === 3) {
            throw 'PAGE_NOT_FOUND';
          } else if (checkPost.error.code === 5) {
            throw 'SMALL_CONTENT';
          } else {
            throw checkPost;
          }
        } else if (checkPost.result.exists) {
          throw {id: checkPost.result.short_id};
        }

        const draft = await $Posts.createDraft(user_id, {
          path,
          account_id: _.get(checkPost, 'result.account.id')
        });
        draftId = draft._id;

        return checkPost.result.language;
      })
      .then(async (language) => {
        let messageText;

        if (language && language !== 'other') {
          messageText = `Publication language: ${hashLanguage[language]}?`;
        } else {
          messageText = `Could not determine the language, help us!`;
        }

        const CMD_PREFIX = `set.draft.language${CMD_SEPARATOR}${draftId}${CMD_SEPARATOR}`;
        let keyboard;

        if (language && language !== 'other') {
          keyboard = [
            Markup.callbackButton('Yes', CMD_PREFIX + language),
            Markup.callbackButton('No', CMD_PREFIX + 'other'),
          ];
        } else {
          keyboard = Object.keys(hashLanguage).reduce((r, code) => {
            let arrLine = r[r.length - 1];

            if (arrLine.length === 3) {
              r.push([]);
              arrLine = r[r.length - 1];
            }

            arrLine.push( Markup.callbackButton(hashLanguage[code], CMD_PREFIX + code) );

            return r;
          }, [
            [  ]
          ]);
        }

        const extra = Extra
          .inReplyTo(ctx.message.message_id)
          .load(Markup.inlineKeyboard(keyboard).extra());

        return telegram.sendMessage(query.user.id, messageText, extra);
      })
      .catch((err) => {
        if (err === 'PAGE_NOT_FOUND') {
          return ctx.reply('This page is not found!');
        } else if (err === 'POST_IS_DRAFT') {
          return ctx.reply('The publication is already someone\'s draft');
        } else if (err === 'SMALL_CONTENT') {
          return ctx.reply('This post is too small to publish.');
        } else if (_.get(err, 'id')) {
          const link = `${shortHost}/${err.id}`;
          return ctx.reply(link, Markup.inlineKeyboard([
            {
              text: 'Share',
              switch_inline_query: link
            }
          ]).extra());
        }

        $log.error(err);
        ctx.reply('Oops.. Happened error!');
      });

  });
};
