const $log = require('../libs/log');

module.exports = function (bot) {
  bot.start((ctx) => {
    const update = ctx.update;
    const messageText = update.message.text;
    const payload = messageText.split('/start ')[1];

    const startCMDs = {
      rate: /^(dis)?like(.+)/,
      'group-account-invite': /^gin(.+)/,
      auth: /^auth/,
      share: /^share(.+)/,
      'from-tj': '^tj$'
    };

    async function startMessage () {
      await ctx.reply('Send any message with a link to Telegra.ph post.');
      await ctx.reply('The post should be larger than 900 characters.');
    }

    if (!payload) {
      startMessage();
    } else {
      let cmd;

      Object.keys(startCMDs).forEach((name) => {
        const match = payload.match(startCMDs[name]);

        if (match) {
          cmd = {
            name,
            match
          };
        }
      });

      if (!cmd) {
        startMessage();
      } else {
        const cmdExecute = require('../start-cmd/' + cmd.name);

        cmdExecute(ctx, cmd.match, {
          startMessage
        })
          .catch((err) => {
            if (err === 0) {
              return;
            }

            $log.error(err);
            ctx.reply('Server Error!');
          });
      }
    }

  });
};