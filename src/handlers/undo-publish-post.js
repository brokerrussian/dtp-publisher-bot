const $log = require('../libs/log');
const $Posts = require('../modules/posts');

const CMD_SEPARATOR = '<!-!>';

module.exports = function (bot) {
  bot.action(/undo.publush.post/, (ctx) => {
    const query = ctx.update.callback_query;
    const payload = query.data.split(CMD_SEPARATOR);
    const draftId = payload[1];

    $Posts.undoPublish(draftId)
      .then(() => {
        ctx.editMessageText('Done! Post deleted from DTP!');
      })
      .catch((err) => {
        $log.error(err);
        ctx.answerCbQuery('Server Error!');
      });
  });
};