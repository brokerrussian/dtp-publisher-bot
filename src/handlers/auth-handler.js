const $Users = require('../modules/users');
const $log = require('../libs/log');
const $config = require('config');
const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

const DTPUrl = $config.get('services.dtp.url');

function handler (ctx) {
  const userId = ctx.message.from.id;

  $Users.updatePhoto(userId);
  $Users.getOpenToken(userId)
  .then(async (openToken) => {
      const url = `${DTPUrl}/auth/${openToken}`;    
      const messageText = `The authorization link is valid once. Logout the account to reset all active sessions.`;

      await ctx.reply(messageText);
      await ctx.reply(url, Extra.load({
        disable_web_page_preview: true
      }));
    })
    .catch((err) => {
      $log.error(err);
      ctx.reply('Server Error!');
    });
}

module.exports = handler;