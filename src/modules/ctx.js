class CTX {
  constructor (ctx) {
    this.ctx = ctx;
  }

  get user () {
    if (this.ctx.from.is_bot) {
      return false;
    }

    return this.ctx.from;
  }

  get bot () {
    if (!this.ctx.from.is_bot) {
      return false;
    }
  }

  get message () {
    return this.ctx.message;
  }

  get chatType () {
    // private, group, supergroup, channel
    return this.ctx.chat.type;
  }

  static parse (ctx) {
    return new CTX(ctx);
  }
}

module.exports = CTX;

// EXAMPLE

// {
//   "from": {
//     "id": 47106923,
//     "is_bot": false,
//     "first_name": "Anton",
//     "last_name": "Danilov",
//     "username": "deviun",
//     "language_code": "root"
//   },
//   "message": {
//     "message_id": 7,
//     "from": {
//       "id": 47106923,
//       "is_bot": false,
//       "first_name": "Anton",
//       "last_name": "Danilov",
//       "username": "deviun",
//       "language_code": "root"
//     },
//     "chat": {
//       "id": 47106923,
//       "first_name": "Anton",
//       "last_name": "Danilov",
//       "username": "deviun",
//       "type": "private"
//     },
//     "date": 1517504390,
//     "text": "/start",
//     "entities": [
//       {
//         "offset": 0,
//         "length": 6,
//         "type": "bot_command"
//       }
//     ]
//   }
// }