const $mongo = require('../libs/mongo');
const $AF = require('../libs/action-flow');
const $uuid = require('uuid/v4');
const $sha256 = require('sha256');
const _ = require('lodash');
const $Telegram = require('telegraf/telegram');
const $config = require('config');

const telegram = new $Telegram($config.get('bot.token'));

class Users {
  static get db () {
    return $mongo.collection('users');
  }

  static async updatePhoto (userId) {
    const userPhotos = await telegram.getUserProfilePhotos(userId, 0, 1);
    const photo = userPhotos.photos[0];

    if (!photo) {
      return;
    }

    await Users.db.editOne({
      id: userId
    }, {
      photo: photo
    });
  }

  static async setSession (userObject) {
    const sessionTime = Math.round((new Date()).getTime() / 1000);
    const AF = $AF.create({
      description: 'set use session',
      user_id: userObject.id
    });

    async function cancel (err, end) {
      await AF.end();

      if (err) {
        throw err;
      }

      return end;
    }

    await AF.await().catch(cancel);
    const user = await Users.getById(userObject.id).catch(cancel);

    if (!user) {
      await Users.create(userObject).catch(cancel);
    }

    await Users.db.editOne({ id: userObject.id }, {
      first_name: userObject.first_name || '',
      last_name: userObject.last_name || '',
      last_bot_session: sessionTime
    }).catch(cancel);

    await AF.end();

    return true;
  }

  static async getById (id) {
    return await Users.db.findOne({ id });
  }

  static async create (userObject) {
    const user = _.pick(userObject, [
      'id', 'first_name', 'last_name'
    ]);

    if (!user.first_name) {
      user.first_name = '';
    }

    if (!user.last_name) {
      user.last_name = '';
    }

    user.openToken = $sha256($uuid());
    user.secretToken = $sha256($uuid());

    return await Users.db.insert(user);
  }

  static async getToken(id) {
    const user = await Users.getById(id);
    let token = _.get(user, 'secretToken');

    if (!token) {
      token = $sha256($uuid());

      await Users.db.editOne({ id }, {
        secretToken: token
      });
    }

    return token;
  }

  static async getOpenToken (id) {
    const user = await Users.getById(id);
    let token = _.get(user, 'openToken');

    if (!token) {
      token = $sha256($uuid());

      await Users.db.editOne({ id }, {
        openToken: token
      });
    }

    return token;
  }
}

module.exports = Users;
