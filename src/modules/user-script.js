const $mongo = require('../libs/mongo');

class UserScript {
  static get db () {
    return $mongo.collection('userScript');
  }

  static async get (user_id) {
    return await UserScript.db.findOne({ user_id });
  }

  static async set (user_id, cmd, payload) {
    await UserScript.cancel(user_id);

    const insert = {
      user_id,
      cmd
    };

    if (payload) {
      insert.payload = payload;
    }

    return await UserScript.db.insert(insert);
  }

  static async cancel (user_id) {
    return await UserScript.db.deleteMany({ user_id });
  }
}

module.exports = UserScript;